#!/usr/bin/env python

import os
import sys
import json
from itertools import groupby

tables = {'genre': {'caption': "Genre Classification",
                    'label': "genre",
                    'result_type': "Accuracy (std. dev.)",
                    'prefix': "out/homburg",
                    },
          'sample': {'caption': "Sample Identification",
                     'label': "sample",
                     'result_type': "MAP (std. dev.)",
                     'prefix': "out/vanbalen",
          }
}

model_types = {'LDA_10_GAUSS': "GaussLDA-10",
               'LDA_50_GAUSS': "GaussLDA-50",
               'HDP_100_GAUSS': "GaussHDP-100",
               'LDA_50_MULT': "LDA-50",
               'cnn': "CNN"
               }

feature_types = {'GFCCs': "GFCC",
                 'Scale': "Scale",
                 'GFCCsScale': "GFCC + Scale",
                 'GFCCsKmeans': "GFCC K-means",
                 'CNN': "CNN",
}

feature_key = {'GFCCs': "A",
               'Scale': "B",
               'GFCCsScale': "C",
               'GFCCsKmeans': "D",
               'CNN': "E",
}

def main():
    filenames = sys.argv[1:3]
    for fname, x in zip(filenames, ['genre', 'sample']):
        with open(fname, 'w') as f:
            f.write(compute_table(x))

def compute_table(table_type):
    table = tables[table_type]
    results = {}
    for model_type in model_types.keys():
        for feature_type in feature_types.keys():
            results.update(extract_results(table, model_type, feature_type))
    sorted_result_list = toggle_max([results[k] for k in sorted(results.keys())])
    sorted_results = []
    first_group = True
    for k, group in groupby(sorted_result_list, table_key):
        if first_group:
            first_group = False
        else:
            sorted_results.append('\\midrule')
        group = u' \\\\\n    '.join(map(format_result, list(group))) + ' \\\\'
        sorted_results.append(group)
    table['results'] = u'\n    '.join(sorted_results)
    return format_table(table)

def table_key(k):
    ks = k['key'].split('-')
    return ks[0]

def extract_results(table, model_type, feature_type):
    filename = '{}_{}_{}.json'.format(table['prefix'], model_type, feature_type)

    if model_type == 'LDA_50_MULT' and feature_type == 'GFCCsKmeans':
        filename = table['prefix'] + '_kmeans_GFCCsKmeans.json'

    results = {}
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            try:
                result_obj = json.load(f)
            except Exception, e:
                print("{} could not be decoded".format(filename))
                raise e
            if feature_type == 'CNN':
                if model_type == 'cnn':
                    result_obj.pop('cnn', None)
                else:
                    result_obj.pop('mean_svm', None)
                    result_obj.pop('mean_cos', None)
            for strategy, value in result_obj.iteritems():
                model = model_name(model_type, strategy)
                features = feature_types[feature_type] if strategy != 'baseline' else '---'
                key = result_key(model_type, feature_key[feature_type], strategy)
                result = {'model': model, 'features': features, 'value': value['mean'], 'std': value['std'], 'is_top': False, 'key': key}
                results[key] = result
    return results

def toggle_max(result_list):
    value_max = 0.0
    i_max = -1
    for i, result in enumerate(result_list):
        if result['value'] > value_max:
            value_max = result['value']
            i_max = i
    result_list[i_max]['is_top'] = True
    return result_list

def model_name(model_type, strategy):
    name = ''
    if strategy.startswith('topics'):
        name = model_types[model_type] + ' + '
    elif strategy.startswith('mean'):
        name = 'Mean + '
    elif strategy == 'baseline':
        name = 'Random'

    if strategy.endswith('svm'):
        name += 'SVM'
    elif strategy.endswith('cos'):
        name += 'Cos'
    return name

def result_key(model_type, feature_type, strategy):
    if model_type == 'cnn':
        model_type = 'zcnn'  # put at end
    key = model_type + feature_type + strategy
    if strategy.startswith('topics'):
        if model_type.startswith('HDP'):
            key = 'topicsLDA_50_GAUSS_' + '-' + feature_type
        else:
            key = 'topics' + model_type + '-' + feature_type
    elif strategy.startswith('mean'):
        key = 'mean-' + feature_type
    elif strategy == 'baseline':
        key = '_baseline'
    elif strategy == 'kmeans_svm':
        key = 'topicsLDA_50_MULT-C'
    return key

def format_result(result):
    value_str = '{:.2f}'.format(result['value'] * 100.0)
    std_str = ' ({:.2f})'.format(result['std'] * 100.0)
    if result['is_top']:
        value_str = '\\textbf{' + value_str + '}'
    result_str = "{model} & {features} & ".format(**result) + value_str + std_str
    return result_str

def format_table(table):
    return """
  \\caption{{{caption}}}
  \\label{{{label}-table}}
  \\centering
  \\begin{{tabular}}{{lll}}
    \\toprule
    Model    & Feature(s)     & {result_type} \\\\
    \\midrule
    {results}
    \\bottomrule
  \\end{{tabular}}
""".format(**table)

if __name__ == '__main__':
    main()
