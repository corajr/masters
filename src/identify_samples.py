#!/usr/bin/env python

import sys
import bnpy
import h5py
import numpy as np
from bnpy_util import topic_proportions_for_tracks
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import KFold
from sklearn.svm import SVC
from ml_metrics.average_precision import mapk
from vanbalen_samples import get_sample_metadata, sample_or_original
from experiment_util import run_experiment, get_kmeans_bof, get_mean_of_feature
from sklearn.externals import joblib

def strategies_for_dtype(dtype):
    if dtype == 'GFCCsKmeans':
        return {'kmeans_cos': "K-means + cosine similarity",
                'kmeans_lda_cos': "Topics from K-means + Cosine similarity",
                # 'kmeans_ovr': "K-means + One-vs-rest linear SVM"
        }
    else:
        return {'topics_cos': "Topic + Cosine similarity",
                'mean_cos': "Mean of feature + cosine similarity",
                # 'ovr_svm': "One-vs-rest linear SVM"
        }


def evaluate_sample_task(train_tracks, test_tracks, X_train, X_test,
                         strategy):
    metadata = get_sample_metadata()
    relations = metadata['relations']
    track_types = sample_or_original()
    candidates = [x for x in track_types.keys() if track_types[x] == 1]
    queries = [x for x in track_types.keys() if track_types[x] == 0]
    all_tracks = np.array(train_tracks + test_tracks)

    candidates_i = np.in1d(all_tracks, candidates)
    queries_i = np.in1d(all_tracks, queries)

    query_list = all_tracks[queries_i]
    candidate_list = all_tracks[candidates_i]
    X_all = np.vstack((X_train, X_test))

    X_candidates = X_all[candidates_i, :]
    X_queries = X_all[queries_i, :]

    y_true = [relations.get(x, []) for x in query_list]

    if strategy.endswith('cos'):
        y_hat = cosine_similarity(X_queries, X_candidates)
        result = mapk(y_true,
                      similarity_to_sorted_labels(y_hat, candidate_list))
    return result


def identify_samples(model_path, track_file, strategy, out_f, data_type):
    model = bnpy.load_model(model_path)

    with h5py.File(track_file, 'r') as f:
        train_data = f['train']
        train_tracks = train_data.keys()
        test_data = f['test']
        test_tracks = test_data.keys()

        if data_type == 'GFCCsKmeans':
            kmeans_file = track_file.replace('.h5', '_GFCCs_kmeans.pkl')
            kmeans = joblib.load(kmeans_file)
            if strategy == 'kmeans_cos' or strategy == 'kmeans_ovr':
                X_train = get_kmeans_bof(data=train_data, dtype=data_type, kmeans=kmeans)
                X_test = get_kmeans_bof(data=test_data, dtype=data_type, kmeans=kmeans)
            elif strategy == 'kmeans_lda_cos':
                X_train = topic_proportions_for_tracks(model, train_data, train_tracks, f, data_type, kmeans)
                X_test = topic_proportions_for_tracks(model, test_data, test_tracks, f, data_type, kmeans)
        else:
            if strategy == 'topics_cos':
                X_train = topic_proportions_for_tracks(model, train_data, train_tracks, f, data_type)
                X_test = topic_proportions_for_tracks(model, test_data, test_tracks, f, data_type)
            elif strategy == 'mean_cos':
                X_train = get_mean_of_feature(train_data, data_type)
                X_test = get_mean_of_feature(test_data, data_type)

        result = evaluate_sample_task(train_tracks, test_tracks,
                                      X_train, X_test, strategy)

    return result

def similarity_to_sorted_labels(y, ls):
    out = []
    for row in y:
        this_row_labels = []
        for i in np.argsort(row)[::-1]:
            if row[i] > 0.0:
                this_row_labels.append(ls[i])
        out.append(this_row_labels)
    return out

def baseline(queries_N=75, candidates_N=68):
    relations = get_sample_metadata()['relations']
    data = sample_or_original()
    query_list = [x for x in data.keys() if data[x] == 0]
    candidate_list = [x for x in data.keys() if data[x] == 1]
    y_true = [relations.get(x, []) for x in query_list]
    similarity = np.random.rand(queries_N, candidates_N)
    return mapk(y_true, similarity_to_sorted_labels(similarity, candidate_list))

def main():
    run_experiment(sys.argv, baseline, identify_samples, strategies_for_dtype)

if __name__ == '__main__':
    main()
