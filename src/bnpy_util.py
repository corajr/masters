#!/usr/bin/env python

import bnpy.data.GroupXData as GroupXData
import bnpy.data.WordsData as WordsData
import numpy as np
import h5py
from sklearn.preprocessing import normalize
from scipy.fftpack import dct

def topic_proportions_for_tracks(model, grp, track_names, track_file, data_type, kmeans=None):
    if data_type == 'GFCCsKmeans':
        Data = track_names_to_words_data(grp, track_names, track_file, data_type, kmeans)
    else:
        Data = track_names_to_group_x_data(grp, track_names, track_file, data_type)
    return compute_doc_topics(model, Data)

def mk_scale_from_array(scale_dset):
    scale = []
    for d in np.abs(scale_dset):
        d = dct(d, type=2, norm='ortho')
        d = d[1:60]
        scale.append(d)
    scale = np.vstack(scale)
    return scale

def repeat_scale(scale, gfccs_shape_0):
    scale_repeated = np.repeat(scale, repeats=gfccs_shape_0 // scale.shape[0], axis=0)
    if scale_repeated.shape[0] < gfccs_shape_0:
        n = gfccs_shape_0 - scale_repeated.shape[0]
        last = scale[-1, :]
        scale_repeated = np.vstack((scale_repeated, np.broadcast_to(last, (n, scale.shape[1]))))
    return scale_repeated

def combine_scale_and_gfcc(scale, gfccs):
    scale_repeated = repeat_scale(scale, gfccs.shape[0])
    return np.hstack((gfccs, scale_repeated))

def track_names_to_group_x_data(grp, tracks, track_file, data_type):
    obs = []
    doc_range = [0]
    count = 0
    for track_name in tracks:
        if data_type == 'GFCCs':
            data = grp[track_name]['lowlevel.gfcc'].value
        elif data_type == 'Scale':
            data = repeat_scale(mk_scale_from_array(grp[track_name]['scale'].value), grp[track_name]['lowlevel.gfcc'].shape[0])
        elif data_type == 'GFCCsScale':
            gfccs = grp[track_name]['lowlevel.gfcc'].value
            scale = mk_scale_from_array(grp[track_name]['scale'].value)
            data = combine_scale_and_gfcc(scale, gfccs)
        elif data_type == 'GammaCRNN':
            data = track_file['crnn'][track_name]['gamma.crnn'].value
        elif data_type == 'CNN':
            data = grp[track_name]['cnn.hypercolumn'].value
        count += data.shape[0]
        doc_range.append(count)
        obs.append(data.astype(np.float64))
    X = np.vstack(obs)
    Data = GroupXData(X=X, doc_range=doc_range)
    return Data

def track_names_to_words_data(grp, tracks, track_file, data_type, kmeans):
    ids = []
    counts = []
    doc_range = [0]
    count = 0

    for track_name in tracks:
        t_grp = grp[track_name]
        if data_type == 'GFCCsKmeans':
            data = t_grp['lowlevel.gfcc'].value.astype(np.float64)
            clusters = kmeans.predict(data)
            t_ids, t_counts = np.unique(clusters, return_counts=True)
            count += t_ids.shape[0]
            doc_range.append(count)
            ids.append(t_ids)
            counts.append(t_counts)
        word_id = np.hstack(ids)
        word_count = np.hstack(counts)
        vocab_size = np.amax(word_id) + 1
    Data = WordsData(word_id=word_id, word_count=word_count, doc_range=doc_range, vocab_size=vocab_size)

    return Data


def compute_doc_topics(model, Data):
    LP = model.calc_local_params(Data)
    docTopicCount = LP['DocTopicCount']
    docTopicProportions = normalize(docTopicCount, norm='l1')
    return docTopicProportions
