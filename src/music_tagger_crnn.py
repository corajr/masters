# -*- coding: utf-8 -*-
'''MusicTaggerCRNN model for Keras.

# Reference:

- [Music-auto_tagging-keras](https://github.com/keunwoochoi/music-auto_tagging-keras)

'''
from __future__ import print_function
from __future__ import absolute_import

import math

from keras import backend as K
from keras.layers import Input, Dense
from keras.models import Model, Sequential
from keras.layers import Dense, Dropout, Reshape, Permute
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D, ZeroPadding2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import ELU
from keras.layers.recurrent import GRU
from keras.utils.data_utils import get_file
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.applications.audio_conv_utils import decode_predictions, preprocess_input

TH_WEIGHTS_PATH = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.3/music_tagger_crnn_weights_tf_kernels_th_dim_ordering.h5'
TF_WEIGHTS_PATH = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.3/music_tagger_crnn_weights_tf_kernels_tf_dim_ordering.h5'


def MusicTaggerCRNN(weights='msd', input_shape=None,
                    include_top=True):
    '''Instantiate the MusicTaggerCRNN architecture,
    optionally loading weights pre-trained
    on Million Song Dataset. Note that when using TensorFlow,
    for best performance you should set
    `image_dim_ordering="tf"` in your Keras config
    at ~/.keras/keras.json.

    The model and the weights are compatible with both
    TensorFlow and Theano. The dimension ordering
    convention used by the model is the one
    specified in your Keras config file.

    For preparing mel-spectrogram input, see
    `audio_conv_utils.py` in [applications](https://github.com/fchollet/keras/tree/master/keras/applications).
    You will need to install [Librosa](http://librosa.github.io/librosa/)
    to use it.

    # Arguments
        weights: one of `None` (random initialization)
            or "msd" (pre-training on ImageNet).
        input_shape: Tuple of `(channels, rows (freq), cols (time))`
        include_top: whether to include the 1 fully-connected
            layer (output layer) at the top of the network.
            If False, the network outputs 32-dim features.


    # Returns
        A Keras model instance.
    '''
    if weights not in {'msd', None}:
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) or `msd` '
                         '(pre-training on Million Song Dataset).')

    # Determine proper input shape
    if input_shape is None:
        if K.image_dim_ordering() == 'th':
            input_shape = (1, 96, 1366)
        else:
            input_shape = (96, 1366, 1)

    # Determine input axis
    if K.image_dim_ordering() == 'th':
        channel_axis = 1
        freq_axis = 2
        time_axis = 3
    else:
        channel_axis = 3
        freq_axis = 1
        time_axis = 2

    freq_dim = input_shape[freq_axis - 1]
    time_dim = input_shape[time_axis - 1]

    # in order for pooling to work,
    # must have 2*3*4*4 == 96 frequency bands
    assert freq_dim == 96, "Frequency dimension was {} but needed 96".format(freq_dim)

    x_dim = int(math.ceil(time_dim * 1.0 / 96))

    time_pad = (x_dim*96) - time_dim
    time_pad_L = time_pad // 2
    time_pad_R = time_pad - time_pad_L

    # Input block
    model = Sequential()
    if time_pad_L != 0 or time_pad_R != 0:
        model.add(ZeroPadding2D(padding=(0, 0, time_pad_L, time_pad_R),
                                input_shape=input_shape))
        model.add(BatchNormalization(axis=time_axis, name='bn_0_freq'))
    else:
        model.add(BatchNormalization(axis=time_axis, name='bn_0_freq',
                                     input_shape=input_shape))

    # Conv block 1
    model.add(Convolution2D(64, 3, 3, border_mode='same', name='conv1'))
    model.add(BatchNormalization(axis=channel_axis, mode=0, name='bn1'))
    model.add(ELU())
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), name='pool1'))

    # Conv block 2
    model.add(Convolution2D(128, 3, 3, border_mode='same', name='conv2'))
    model.add(BatchNormalization(axis=channel_axis, mode=0, name='bn2'))
    model.add(ELU())
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(3, 3), name='pool2'))

    # Conv block 3
    model.add(Convolution2D(128, 3, 3, border_mode='same', name='conv3'))
    model.add(BatchNormalization(axis=channel_axis, mode=0, name='bn3'))
    model.add(ELU())
    model.add(MaxPooling2D(pool_size=(4, 4), strides=(4, 4), name='pool3'))

    # Conv block 4
    model.add(Convolution2D(128, 3, 3, border_mode='same', name='conv4'))
    model.add(BatchNormalization(axis=channel_axis, mode=0, name='bn4'))
    model.add(ELU())
    model.add(MaxPooling2D(pool_size=(4, 4), strides=(4, 4), name='pool4'))

    # reshaping
    if K.image_dim_ordering() == 'th':
        model.add(Permute((3, 1, 2)))

    model.add(Reshape((x_dim, 128)))

    # GRU block 1, 2, output
    model.add(GRU(32, return_sequences=True, name='gru1'))

    if include_top:
        model.add(GRU(32, return_sequences=False, name='gru2'))

    if weights is None:
        return model
    else:
        # Load weights
        if K.image_dim_ordering() == 'tf':
            weights_path = get_file('music_tagger_crnn_weights_tf_kernels_tf_dim_ordering.h5',
                                    TF_WEIGHTS_PATH,
                                    cache_subdir='models')
        else:
            weights_path = get_file('music_tagger_crnn_weights_tf_kernels_th_dim_ordering.h5',
                                    TH_WEIGHTS_PATH,
                                    cache_subdir='models')
        model.load_weights(weights_path, by_name=True)
        if K.backend() == 'theano':
            convert_all_kernels_in_model(model)
        return model
