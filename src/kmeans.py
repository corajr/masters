#!/usr/bin/env python

from split import do_kmeans
import sys
import h5py


def all_kmeans(split_files):
    for split_file in split_files:
        with h5py.File(split_file, 'r') as f:
            do_kmeans(f['train'], split_file)


if __name__ == '__main__':
    split_files = sys.argv[1:]
    all_kmeans(split_files)
