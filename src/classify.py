#!/usr/bin/env python

import bnpy
from bnpy_util import topic_proportions_for_tracks
import h5py
import numpy as np
import sys
import json
from sklearn import svm
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix, accuracy_score
from group_classes import determine_genres, genre_to_number
from sklearn.externals import joblib
from experiment_util import run_experiment, get_kmeans_bof, get_mean_of_feature

def strategies_for_dtype(dtype):
    if dtype == 'GFCCsKmeans':
        return {'kmeans_svm': 'K-means + Linear SVM',
                'topics_svm': 'Topics from K-means + Linear SVM'}
    else:
        return {'topics_svm': 'Topics + Linear SVM',
                'mean_svm': "Mean of feature + Linear SVM"
        }

def genre_cluster(model_path, track_file, strategy, out_f, data_type='GFCCs'):
    model = bnpy.load_model(model_path)
    track_genres = determine_genres()
    genre_numbers = genre_to_number()

    results = {}

    with h5py.File(track_file, 'r') as f:
        train_data = f['train']
        train_tracks = train_data.keys()
        y = np.asarray([genre_numbers[track_genres[x]] for x in train_tracks])

        test_data = f['test']
        test_tracks = test_data.keys()
        y_true = np.asarray([genre_numbers[track_genres[x]] for x in test_tracks])

        if data_type == 'GFCCsKmeans':
            kmeans_file = track_file.replace('.h5', '_GFCCs_kmeans.pkl')
            kmeans = joblib.load(kmeans_file)
            if strategy == 'kmeans_svm':
                X_train = get_kmeans_bof(data=train_data, dtype=data_type, kmeans=kmeans)
                X_test = get_kmeans_bof(data=test_data, dtype=data_type, kmeans=kmeans)
            elif strategy == 'topics_svm':
                X_train = topic_proportions_for_tracks(model, train_data, train_tracks, f, data_type, kmeans)
                X_test = topic_proportions_for_tracks(model, test_data, test_tracks, f, data_type, kmeans)
        else:
            if strategy == 'topics_svm':
                X_train = topic_proportions_for_tracks(model, train_data, train_tracks, f, data_type)
                X_test = topic_proportions_for_tracks(model, test_data, test_tracks, f, data_type)
            elif strategy == 'mean_svm':
                X_train = get_mean_of_feature(train_data, data_type)
                X_test = get_mean_of_feature(test_data, data_type)

        clf = svm.SVC(kernel='linear')
        clf.fit(X_train, y)
        y_hat = clf.predict(X_test)

        results['accuracy'] = accuracy_score(y_true, y_hat)

    return results['accuracy']

def baseline(tracks_N=192, genres_N=9):
    track_genres = determine_genres()
    genre_numbers = genre_to_number()
    tracks = np.random.choice(track_genres.keys(), size=(tracks_N))
    y_true = [genre_numbers[track_genres[x]] for x in tracks]
    guesses = np.random.randint(genres_N, size=tracks_N)
    return accuracy_score(y_true, guesses)

def main():
    run_experiment(sys.argv, baseline, genre_cluster, strategies_for_dtype)

if __name__ == '__main__':
    main()
