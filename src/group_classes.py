#!/usr/bin/env python

from os.path import join
import sys
import os
import json
from collections import defaultdict

def determine_genres(write_to='out/homburg_genres.json'):
    if os.path.exists(write_to):
        with open(write_to, 'r') as f:
            return json.load(f)
    else:
        genres = {}
        data_dir = join('data', 'homburg')
        for genre in os.listdir(data_dir):
            genres[genre] = os.listdir(join(data_dir, genre))
        track_genres = transpose_dict(genres)
        with open(write_to, 'wb') as f:
            json.dump(track_genres, f)

def genre_to_number():
    track_genres = determine_genres()
    genres = set(track_genres.values())
    genre_numbers = {}
    for i, genre in enumerate(genres):
        genre_numbers[genre] = i
    return genre_numbers

def transpose_dict(d):
    result = {}
    for k, v in d.iteritems():
        for v1 in v:
            result[v1] = k
    return result

def group_by_genre(track_names):
    track_genres = determine_genres()
    by_genre = defaultdict(list)
    for track in track_names:
        genre = track_genres[track]
        by_genre[genre].append(track)
    return dict(by_genre)

if __name__ == '__main__':
    genre_json_path = sys.argv[1]
    determine_genres(genre_json_path)
