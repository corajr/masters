#!/usr/bin/env python


import sys
import h5py
import numpy as np
import os.path
from progressbar import ProgressBar
from nn import extract_cnn_features, extract_cnn_hypercolumn
import librosa

CNN_FEATURE = 'feature.cnn'
CNN_HYPERCOLUMN = 'cnn.hypercolumn'


def add_cnn_features(track_file, files, recompute=set([])):
    pbar = ProgressBar()

    with h5py.File(track_file) as f:
        for filename in pbar(files):
            basename = os.path.basename(filename)
            grp = f[basename]
            for feat in recompute:
                if feat in grp:
                    del grp[feat]
            if CNN_FEATURE or CNN_HYPERCOLUMN not in grp:
                x, _ = librosa.load(filename, sr=12000, duration=29.0)
                x = librosa.util.fix_length(x, 12000*29, mode='wrap')
                if CNN_FEATURE not in grp:
                    feats = extract_cnn_features(x)
                    grp.create_dataset(CNN_FEATURE,
                                       data=feats)
                if CNN_HYPERCOLUMN not in grp:
                    feats = extract_cnn_hypercolumn(x)
                    grp.create_dataset(CNN_HYPERCOLUMN,
                                       data=feats)


def main():
    track_file = sys.argv[1]
    files = sys.argv[2:]
    add_cnn_features(track_file, files)


if __name__ == '__main__':
    main()
