#!/usr/bin/env python

import os
import sys
import json
import csv
import librosa
from collections import defaultdict
from intervaltree import IntervalTree
import cPickle as pickle


def get_sample_metadata(write_to='out/vanbalen_samples.json',
                        csv_input='data/vanbalen/samples.csv'):
    if os.path.exists(write_to):
        with open(write_to, 'r') as f:
            return json.load(f)
    else:
        metadata = {'original': {}, 'relations': defaultdict(list)}

        with open(csv_input, 'rU') as f:
            reader = Iso8859DictReader(f)
            for row in reader:
                sample_id = row.pop('sample_id')
                metadata['original'][sample_id] = row

        for sample_id, sample_info in metadata['original'].iteritems():
            old_track = id_to_filename(sample_info['original_track_id'])
            sampling_track = id_to_filename(sample_info['sample_track_id'])
            metadata['relations'][sampling_track].append(old_track)
        metadata['relations'] = dict(metadata['relations'])

        with open(write_to, 'w') as f:
            json.dump(metadata, f)
        return metadata


def sample_or_original():
    """ Sample (new track/query) = 0; old track/candidate = 1"""
    metadata = get_sample_metadata()
    all_tracks = set()
    original = set()
    sample = set()
    for new_track, original_tracks in metadata['relations'].iteritems():
        sample.add(new_track)
        original.update(original_tracks)
        all_tracks.update([new_track] + original_tracks)
    return {track: 0 if track in sample else 1 for track in all_tracks}


def sample_intervals(out_file='out/vanbalen_sample_intervals.pkl'):
    if os.path.exists(out_file):
        with open(out_file, 'rb') as f:
            return pickle.load(f)
    else:
        metadata = get_sample_metadata()
        prefix = 'data/vanbalen/'
        intervals = defaultdict(IntervalTree)
        for sample_id, sample_info in metadata['original'].iteritems():
            old_track = id_to_filename(sample_info['original_track_id'])
            sampling_track = id_to_filename(sample_info['sample_track_id'])
            t_original = int(sample_info['t_original'])
            t_sample = int(sample_info['t_sample'])
            dur_original = int(librosa.get_duration(filename=prefix +
                                                    old_track))
            dur_sample = int(librosa.get_duration(filename=prefix +
                                                  sampling_track))
            intervals[old_track][t_original:dur_original] = [old_track]
            intervals[sampling_track][t_sample:dur_sample] = [old_track]

        intervals = dict(intervals)
        with open(out_file, 'wb') as f:
            pickle.dump(intervals, f)
        return intervals


def get_samples_for_interval(intervals, name, start, end):
    tree = intervals[name]
    results = sorted(tree[start:end])
    return [item for interval in results for item in interval.data]


def Iso8859DictReader(iso8859_data, **kwargs):
    csv_reader = csv.DictReader(iso8859_data, **kwargs)
    for row in csv_reader:
        yield {key: unicode(value, 'iso-8859-1')
               for key, value in row.iteritems()}


def id_to_filename(track_id):
    return '22k_' + track_id + '.wav'


def main():
    [sample_json, sample_csv] = sys.argv[1:3]
    get_sample_metadata(sample_json, sample_csv)
    sample_intervals()


if __name__ == '__main__':
    main()
