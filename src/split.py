#!/usr/bin/env python

import sys
import h5py
import numpy as np
from group_classes import determine_genres
from vanbalen_samples import sample_or_original
from sklearn.model_selection import StratifiedKFold
from sklearn.cluster import MiniBatchKMeans
from sklearn.externals import joblib

def split(original, split_files, seed=0):
    with h5py.File(original, 'r') as f:
        tracks = f.keys()
        classes = None
        if original.endswith('homburg.h5'):
            classes = determine_genres()
        elif original.endswith('vanbalen.h5'):
            classes = sample_or_original()
        track_classes = [classes[x] for x in tracks]
        kf = StratifiedKFold(n_splits=10, shuffle=True, random_state=seed)
        for i, (train_indices, test_indices) in enumerate(kf.split(tracks, track_classes)):
            with h5py.File(split_files[i], 'w') as out:
                train_grp = out.create_group('train')
                for j in train_indices:
                    link_data(tracks, f, train_grp, j)
                test_grp = out.create_group('test')
                for j in test_indices:
                    link_data(tracks, f, test_grp, j)

def link_data(tracks, f, new_grp, i):
    track_name = tracks[i]
    old_grp = f[track_name].name
    new_grp[track_name] = h5py.ExternalLink(f.filename, old_grp)

def do_kmeans(train_grp, fname, dtype = 'GFCCs', n_clusters=2000):
    fname = fname.replace('.h5', '_' + dtype + '_kmeans.pkl')
    data = []
    if dtype == 'GFCCs':
        for k in train_grp.keys():
            data.append(train_grp[k]['lowlevel.gfcc'])
    data = np.vstack(data)
    kmeans = MiniBatchKMeans(n_clusters=n_clusters)
    kmeans.fit(data)
    joblib.dump(kmeans, fname)

if __name__ == '__main__':
    original = sys.argv[1]
    split_files = sys.argv[2:]
    split(original, split_files)
