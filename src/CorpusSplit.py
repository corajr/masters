''' CorpusSplit.py
    Gets a different split depending on filename.
'''
import bnpy.data.GroupXData as GroupXData
import bnpy.data.WordsData as WordsData
from src.bnpy_util import track_names_to_group_x_data, track_names_to_words_data
import h5py
import numpy as np
import re
import os
from os.path import join
from sklearn.externals import joblib

[CORPUS_NAME, FEATURE_TYPE, SPLIT_N] = re.search(r'(\w+)CorpusSplit(\w+)(\d+)\.py', __file__).groups()
corpus = CORPUS_NAME.lower()
DATA_DIR = 'out/' + corpus + '_splits'
DATAFILE_MAT = join(DATA_DIR, corpus + FEATURE_TYPE + SPLIT_N + '.mat')
CORPUS_H5 = join (DATA_DIR, corpus + SPLIT_N + '.h5')

def get_data(**kwargs):
    ''' Returns data from audio tracks
    '''
    if FEATURE_TYPE == 'GFCCsKmeans':
        return get_data_kmeans(**kwargs)

    if os.path.exists(DATAFILE_MAT):
        Data = GroupXData.LoadFromFile(DATAFILE_MAT)
    else:
        obs = []
        doc_range = [0]
        count = 0
        with h5py.File(CORPUS_H5, 'r') as f:
            grp = f['train']
            Data = track_names_to_group_x_data(grp, grp.keys(), f, FEATURE_TYPE)
        Data.save_to_mat(DATAFILE_MAT)
    Data.name = CORPUS_NAME + 'CorpusSplit' + FEATURE_TYPE + SPLIT_N
    Data.summary = "{} corpus with {} features; split {}".format(CORPUS_NAME, FEATURE_TYPE, SPLIT_N)

    return Data

def get_data_kmeans(**kwargs):
    if os.path.exists(DATAFILE_MAT):
        Data = WordsData.LoadFromFile_tokenlist(DATAFILE_MAT, vocab_size=2000)
    else:
        kmeans_file = CORPUS_H5.replace('.h5', '_GFCCs_kmeans.pkl')
        kmeans = joblib.load(kmeans_file)

        with h5py.File(CORPUS_H5, 'r') as f:
            grp = f['train']
            Data = track_names_to_words_data(grp, grp.keys(), f, FEATURE_TYPE, kmeans)
        Data.WriteToFile_tokenlist(DATAFILE_MAT)
    Data.name = CORPUS_NAME + 'CorpusSplit' + FEATURE_TYPE + SPLIT_N
    Data.summary = "{} corpus with {} features; split {}".format(CORPUS_NAME, FEATURE_TYPE, SPLIT_N)

    return Data


if __name__ == '__main__':
    get_data()
