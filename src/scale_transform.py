#!/usr/bin/env python

import librosa
import sys
import os
import h5py
import numpy as np
from progressbar import ProgressBar


def scale_transform(audio_filename):
    y, sr = librosa.load(audio_filename)
    odf = librosa.onset.onset_strength(y=y, sr=sr)
    secs = librosa.core.get_duration(y=y, sr=sr)
    scales = []
    for start in librosa.time_to_frames(np.arange(0.0, secs, 4.0), sr=sr):
        odf_win = odf[start:(start + (8 * sr // 512))]
        odf_ac = librosa.autocorrelate(odf_win, max_size=8 * sr // 512)
        odf_ac = librosa.util.normalize(odf_ac, norm=np.inf)
        if odf_ac.shape[-1] < 3: # not enough samples
            break

        try:
            odf_ac_scale = librosa.fmt(odf_ac, n_fmt=512)
        except (SystemExit, KeyboardInterrupt):
            break
        except Exception as e:
            break
        scales.append(odf_ac_scale)
    return np.vstack(scales)

def main(recompute_scales=False):
    track_file = sys.argv[1]
    files = sys.argv[2:]

    pbar = ProgressBar()
    with h5py.File(track_file) as f:
        for fname in pbar(files):
            base = os.path.basename(fname)
            grp = f[base]
            if recompute_scales:
                if 'scale' in grp:
                    del grp['scale']
            if 'scale' not in grp:
                odf_ac_scale = scale_transform(fname)
                grp.create_dataset('scale',
                                   data=odf_ac_scale,
                                   compression=9)

if __name__ == '__main__':
    main()
