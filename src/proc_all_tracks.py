#!/usr/bin/env python

import os
from essentia.streaming import *
from scale_transform import scale_transform
from keras.applications.music_tagger_crnn import preprocess_input
import h5py
import logging
import sys
from progressbar import ProgressBar
from constants import (WINDOW_SIZE, HOP_SIZE, SAMPLE_RATE,
                       LOW_FREQ, HIGH_FREQ, BANDS, COEFS)
from beat_spectrum import beat_dcts_from_mfccs


def analyze_tracks(track_file, files, recompute=set()):
    w = Windowing(type='hann', size=WINDOW_SIZE)
    spectrum = Spectrum()
    options = {'sampleRate': SAMPLE_RATE,
               'numberBands': BANDS,
               'numberCoefficients': COEFS,
               'lowFrequencyBound': LOW_FREQ,
               'highFrequencyBound': HIGH_FREQ
               }
    gfcc = GFCC(**options)
    fcc_name = 'lowlevel.gfcc'

    framecutter = FrameCutter(frameSize=WINDOW_SIZE, hopSize=HOP_SIZE)
    peaks = SpectralPeaks(sampleRate=SAMPLE_RATE)
    hpcp = HPCP(sampleRate=SAMPLE_RATE)
    pool = essentia.Pool()

    framecutter.frame >> w.frame >> spectrum.frame
    spectrum.spectrum >> peaks.spectrum
    peaks.magnitudes >> hpcp.magnitudes
    peaks.frequencies >> hpcp.frequencies

    spectrum.spectrum >> gfcc.spectrum
    # rt = RhythmTransform()
    # gfcc.bands >> rt.melBands
    # rt.rhythm >> (pool, 'rhythm')

    spectrum.spectrum >> (pool, 'lowlevel.fft')
    hpcp.hpcp >> (pool, 'lowlevel.hpcp')
    gfcc.gfcc >> (pool, fcc_name)
    gfcc.bands >> (pool, 'lowlevel.gbands')

    loader = MonoLoader()

    loader.audio >> framecutter.signal

    pbar = ProgressBar()

    with h5py.File(track_file, 'a') as f:
        for filename in pbar(files):
            basename = os.path.basename(filename)
            if basename not in f or len(recompute) > 0:
                try:
                    loader.configure(filename=filename,
                                     sampleRate=SAMPLE_RATE)

                    essentia.reset(loader)
                    essentia.run(loader)

                    grp = f.require_group(basename)
                    for k in recompute:
                        if k in grp:
                            del grp[k]
                    for descriptor in pool.descriptorNames():
                        if descriptor not in grp:
                            grp.create_dataset(descriptor,
                                               data=pool[descriptor],
                                               compression=9)
                    if 'crnn.input' not in grp:
                        grp.create_dataset('crnn.input',
                                           data=preprocess_input(filename),
                                           compression=9)
                    if 'scale' not in grp:
                        odf_ac_scale = scale_transform(filename)
                        grp.create_dataset('scale',
                                           data=odf_ac_scale,
                                           compression=9)
                    pool.clear()
                except (SystemExit, KeyboardInterrupt):
                    break
                except Exception as e:
                    # raise e
                    logging.exception(e)

def add_rhythm(track_file='out/vanbalen.h5'):
    progress = ProgressBar()
    with h5py.File(track_file, 'r') as f:
        keys = f.keys()
    for track in progress(keys):
        with h5py.File(track_file) as f:
            grp = f[track]
            if TIMBRE_GROUP in grp and RHYTHM_GROUP not in grp:
                bccs = beat_dcts_from_mfccs(grp[TIMBRE_GROUP])
                grp.create_dataset(RHYTHM_GROUP, data=bccs, compression=9)

if __name__ == '__main__':
    track_file = sys.argv[1]
    files = sys.argv[2:]
    analyze_tracks(track_file, files)
