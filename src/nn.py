#!/usr/bin/env python

import sys
import numpy as np
import scipy as sp
import h5py
import librosa
import logging
from random import shuffle
from constants import (SAMPLE_RATE, HOP_SIZE)
from sklearn import svm
from sklearn.preprocessing import LabelBinarizer, MultiLabelBinarizer
from sklearn.metrics import accuracy_score
from group_classes import determine_genres, genre_to_number
import classify
import identify_samples
from vanbalen_samples import (sample_intervals, sample_or_original,
                              get_samples_for_interval)
from music_tagger_crnn import MusicTaggerCRNN
import keras
from keras.layers import Dense
from keras import backend as K
from keras.backend import image_dim_ordering
from experiment_util import (run_experiment, get_signal_for,
                             basenames_to_filenames)
import compact_cnn.main as cnn

MODELS = {'genre': {'model': None, 'weights': None},
          'sample': {'model': None, 'weights': None},
          'raw': {'model': None, 'weights': None},
          'cnn': {'models': None}}

HYPERCOL = None

def get_model(task, mk_model, output_classes, input_shape=None):
    if MODELS[task]['model'] is not None:
        MODELS[task]['model'].set_weights(MODELS[task]['weights'])
    else:
        MODELS[task]['model'] = mk_model(input_shape, output_classes)
        MODELS[task]['weights'] = MODELS[task]['model'].get_weights()
    return MODELS[task]['model']


def mk_cnn_genre_model(input_shape, output_classes):
    model = cnn.main('feature')
    model.add(Dense(output_classes, activation='softmax'))

    model.compile(optimizer=keras.optimizers.Adam(lr=5e-3),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


def mk_cnn_sample_model(input_shape, output_classes):
    model = cnn.main('feature')
    model.add(Dense(output_classes, activation='sigmoid'))
    model.compile(optimizer=keras.optimizers.Adam(lr=5e-3),
                  loss='binary_crossentropy',
                  metrics=['precision'])
    return model


def get_cnn_models():
    if MODELS['cnn']['models'] is None:
        models = [cnn.main('feature', x) for x in range(4, -1, -1)]
        MODELS['cnn']['models'] = models
    return MODELS['cnn']['models']


def get_epoch_length(data, tracks, chunk_len=12000*96, sr=12000):
    n = 0
    for track in tracks:
        fname = (basenames_to_filenames())[track]
        t = librosa.get_duration(filename=fname)
        samples = t * (sr * 1.0)
        size = max(samples, chunk_len)
        n += int(np.ceil(size * 1.0 / chunk_len))
    return n


def get_sample_generator(data, tracks, intervals, mlb,
                         batch_size=32, chunk_len=12000*29):
    ext_axis = 0 if image_dim_ordering() == 'th' else 3
    input_batch = []
    output_batch = []
    shuffle(tracks)
    for track in tracks:
        y = get_signal_for(track)
        ys = (enumerate(np.array_split(y, chunk_len))
              if y.shape[0] > chunk_len
              else [(0, y)])
        for i, section in ys:
            section = librosa.util.fix_length(section, chunk_len)
            section = np.expand_dims(section, axis=ext_axis)
            pos = np.asarray([i * chunk_len, (i+1) * chunk_len])
            times = librosa.samples_to_time(pos, sr=12000)
            [start, end] = times.tolist()
            samples = get_samples_for_interval(intervals, track,
                                               int(start), int(end))
            input_batch.append(section)
            target = mlb.transform([samples])[0]
            output_batch.append(target)
            if len(input_batch) == batch_size:
                yield (np.array(input_batch), np.vstack(output_batch))
                input_batch = []
                output_batch = []
    if len(input_batch) > 0:
        yield (np.asarray(input_batch), np.vstack(output_batch))


def extract_cnn_features(x):
    """Return the features from compact_cnn.

    Expects 29 seconds of audio sampled at 12000 Hz (array of 348000 samples).
    """
    models = get_cnn_models()
    src = batch_mono_signals([x])
    features = [model.predict(src)[0] for model in models]
    return features


def setup_hypercol():
    global HYPERCOL
    if HYPERCOL is None:
        model = get_cnn_models()[0]
        mel_spec = K.function(model.inputs, [model.layers[0].output])
        layer_idx = [idx for idx, layer in enumerate(model.layers[1].layers) if layer.name == 'elu_5']
        layers = K.function(model.layers[1].inputs, [model.layers[1].layers[i].output for i in layer_idx])
        HYPERCOL = {'model': model, 'mel_spec': mel_spec, 'layers': layers}
    return HYPERCOL


def get_mel_spec(X):
    hypercol = setup_hypercol()
    mel_spec = hypercol['mel_spec']
    X_melspec = mel_spec([X])[0]
    return X_melspec


def extract_hypercolumn(X, scale=False):
    hypercol = setup_hypercol()
    layers = hypercol['layers']
    X_melspec = get_mel_spec(X)
    outputs = layers([X_melspec])  # get feature maps from each conv layer
    hypercolumns = []
    for convmap in outputs:
        for fmap in convmap[0]:
            if scale:
                upscaled = sp.misc.imresize(fmap, size=(96, 1360),
                                            mode="F", interp='bilinear')
                hypercolumns.append(upscaled)
            else:
                hypercolumns.append(fmap)
    return np.asarray(hypercolumns)


def extract_cnn_hypercolumn(x):
    X = batch_mono_signals([x])
    hc = extract_hypercolumn(X)
    return hc.max(axis=1).T

def batch_mono_signals(xs):
    return np.asarray([x[np.newaxis, :] for x in xs])


def get_signals(tracks, truncate=False):
    return batch_mono_signals([get_signal_for(name, truncate=truncate)
                               for name in tracks])


def get_cnn_features(data, keys):
    return np.vstack([data[key]['feature.cnn'].value.reshape(-1)
                      for key in keys])


def cnn_classify(model_path, track_file, strategy, out_f, data_type):
    if strategy == 'cnn':
        model = get_model('genre', mk_cnn_genre_model, 9)
    elif strategy == 'cnn_svm':
        model = get_cnn_models()
    track_genres = determine_genres()
    genre_numbers = genre_to_number()
    lb = LabelBinarizer()
    lb.fit(genre_numbers.values())

    with h5py.File(track_file, 'r') as f:
        train_data = f['train']
        train_tracks = train_data.keys()
        y = [genre_numbers[track_genres[x]]
             for x in train_tracks]

        test_data = f['test']
        test_tracks = test_data.keys()
        y_test = [genre_numbers[track_genres[x]]
                  for x in test_tracks]

        if strategy == 'cnn':
            X = get_signals(train_tracks)
            y = lb.transform(y)
            model.fit(X, y, nb_epoch=5, batch_size=32)
            X_test = get_signals(test_tracks)
            y_test = lb.transform(y_test)
            result = model.evaluate(X_test, y_test)[1]
        elif strategy == 'cnn_svm':
            X_nn = get_cnn_features(train_data, train_tracks)
            clf = svm.SVC(kernel='linear')
            clf.fit(X_nn, y)
            X_test_nn = get_cnn_features(test_data, test_tracks)
            y_hat = clf.predict(X_test_nn)
            result = accuracy_score(y_test, y_hat)

    return result


def cnn_identify_samples(model_path, track_file, strategy, out_f, data_type):
    intervals = sample_intervals()
    track_types = sample_or_original()
    mlb = MultiLabelBinarizer()
    mlb.fit([track_types.keys()])

    with h5py.File(track_file, 'r') as f:
        train_data = f['train']
        train_tracks = train_data.keys()
        train_epoch_length = get_epoch_length(train_data, train_tracks)
        train_gen = get_sample_generator(train_data, train_tracks,
                                         intervals, mlb)
        test_data = f['test']
        test_tracks = test_data.keys()
        test_epoch_length = get_epoch_length(test_data, test_tracks)
        test_gen = get_sample_generator(test_data, test_tracks,
                                        intervals, mlb)

        if strategy == 'cnn':
            model = get_model('sample', mk_cnn_sample_model,
                              output_classes=len(mlb.classes_))

            model.fit_generator(train_gen,
                                samples_per_epoch=train_epoch_length,
                                nb_epoch=1)
            result = model.evaluate_generator(test_gen,
                                              val_samples=test_epoch_length)[1]
        elif strategy == 'cnn_cos':
            X_nn = get_cnn_features(train_data, train_tracks)
            X_test_nn = get_cnn_features(test_data, test_tracks)
            result = identify_samples.evaluate_sample_task(
                train_tracks, test_tracks, X_nn, X_test_nn, 'cos')

    logging.info("precision on split {}: {:.2%}".format(track_file, result))

    return result


def strategies_for_sample_dtype(dtype):
    return {#'cnn': "compact_cnn", # unclear how to train
            'cnn_cos': "compact_cnn + cosine similarity"}


def strategies_for_genre_dtype(dtype):
    return {'cnn': "compact_cnn",
            'cnn_svm': "compact_cnn + SVM"}

def main():
    logging.basicConfig()
    out_file = sys.argv[1]
    splits = sys.argv[2:]
    if 'homburg' in out_file:
        run_experiment([None, out_file] + splits + splits,
                       classify.baseline, cnn_classify, strategies_for_genre_dtype)
    elif 'vanbalen' in out_file:
        run_experiment([None, out_file] + splits + splits,
                       identify_samples.baseline, cnn_identify_samples,
                       strategies_for_sample_dtype)


if __name__ == '__main__':
    main()
