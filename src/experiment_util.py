#!/usr/bin/env python

import sys
import re
import os
import json
import numpy as np
import librosa
from sklearn.preprocessing import normalize
from bnpy_util import mk_scale_from_array, combine_scale_and_gfcc

def get_dtype(filename):
    return re.search(r'_([^._]+)\.json', filename).group(1)

def run_experiment(sys_argv, baseline, compute, strategies_for_dtype):
    out_file = sys_argv[1]
    dtype = get_dtype(out_file)

    paths_then_files = sys_argv[2:]
    n = len(paths_then_files) / 2
    paths, files = [paths_then_files[:n], paths_then_files[n:]]
    path_file_pairs = zip(paths, files)

    baselines = [baseline() for b in xrange(100)]
    strategies = strategies_for_dtype(dtype)
    results = {'baseline': get_mean_std(baselines, 'baseline')}
    for strat, label in strategies.iteritems():
        result = [compute(model_path, track_file, strategy=strat, out_f=sys.stdout, data_type = dtype)
                  for model_path, track_file
                  in path_file_pairs]
        results[strat] = get_mean_std(result, label)
    with open(out_file, 'w') as f:
        json.dump(results, f)

def get_mean_std(ls, label=None):
    return {'mean': np.mean(ls), 'std': np.std(ls), 'label': label}

def get_mean_of_feature(data, dtype):
    out = []
    for k in data.keys():
        if dtype == 'GFCCs':
            t_data = data[k]['lowlevel.gfcc'].value
        if dtype == 'Scale':
            t_data = mk_scale_from_array(data[k]['scale'].value)
        elif dtype == 'GFCCsScale':
            scale = mk_scale_from_array(data[k]['scale'].value)
            gfccs = data[k]['lowlevel.gfcc'].value
            t_data = combine_scale_and_gfcc(scale, gfccs)
        elif dtype == 'CNN':
            # flatten for single 160-d feature
            t_data = np.asarray([data[k]['feature.cnn'].value.reshape(-1)])
        out.append(np.mean(t_data, axis=0))
    return np.vstack(out)

def get_kmeans_bof(data, dtype, kmeans):
    if dtype == 'GFCCsKmeans':
        out = []
        for k in data.keys():
            t_gfcc = data[k]['lowlevel.gfcc'].value.astype(np.float64)
            labels = kmeans.predict(t_gfcc)
            vect = np.bincount(labels, minlength=2000)
            out.append(normalize([vect], norm='l1')[0])
        return np.vstack(out)


BASENAME_TO_FILENAME = None


def basenames_to_filenames():
    global BASENAME_TO_FILENAME
    if BASENAME_TO_FILENAME is None or len(BASENAME_TO_FILENAME) == 0:
        BASENAME_TO_FILENAME = {}
        data_dir = os.path.join(os.path.dirname(__file__), '..', 'data')
        for root, _, files in os.walk(data_dir):
            names = [x for x in files
                     if x.endswith('.wav') or x.endswith('.mp3')]
            for name in names:
                BASENAME_TO_FILENAME[name] = os.path.join(root, name)
    return BASENAME_TO_FILENAME


def get_signal_for(basename, sr=12000, min_duration=29, truncate=False):
    filename = (basenames_to_filenames())[basename]
    dur = min_duration if truncate else None
    y, _ = librosa.load(filename, sr=sr, duration=dur)
    if y.shape[0] < min_duration*sr or truncate:
        y = librosa.util.fix_length(y, min_duration*sr, mode='wrap')
    return y
