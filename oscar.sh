#!/bin/bash

# Request an hour of runtime:
#SBATCH --time=1:00:00

# Default resources are 1 core with 2.8GB of memory per core.

# Use more cores (8):
#SBATCH -c 8

# Specify a job name:
#SBATCH -J MastersJob

# Specify an output file
#SBATCH -o MastersJob-%j.out
#SBATCH -e MastersJob-%j.out

# Run a command

make

