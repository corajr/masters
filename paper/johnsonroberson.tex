\documentclass{article}
\usepackage{hyperref}

\usepackage[final]{nips_2016}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{enumitem}

\title{Content-Based Genre Classification and Sample Recognition Using Topic Models}
\author{
Cora~Johnson-Roberson \\
Department of Computer Science \\
Brown University \\
Providence, RI 02912 \\
\texttt{caj8@cs.brown.edu} \\
\And
Erik Sudderth (Advisor) \\
Department of Computer Science \\
Brown University \\
Providence, RI 02912 \\
\texttt{sudderth@cs.brown.edu}
}

\begin{document}
\maketitle

\begin{abstract}
Audio-related research benefits from analysis methods applicable to multiple tasks, as hand-crafting features and models for each task is time-consuming and difficult. In this paper we investigate the use of probabilistic topic modeling for two distinct audio tasks: genre classification and sample identification. Topic models transform frame-level audio features into a single track-level low-dimensional summary. We find that this topic-space representation produces a usable track-level feature for genre classification, but is not as effective for sample identification.
\end{abstract}

\section{Introduction}
The need to draw inferences from audio data arises in a variety of applications: for example, when recommending new music for online streaming services \cite{van_den_oord_deep_2013}, or analyzing rhythm and timing in computational ethnomusicology \cite{tzanetakis_computational_2007}. Numerous techniques for such tasks have been developed in the field of music information retrieval (MIR); there is great interest in models that can perform well on multiple tasks.

Genre recognition is one of the most well-studied problems in MIR. This work typically uses classification accuracy on a handful of common datasets as a performance metric, although some authors have criticized the experimental design of such studies \cite{sturm_classification_2013}. In particular, success with genre recognition on specific datasets may not generalize well, as genre is complex and context-dependent. Nonetheless, this task comes up in real-world situations like categorizing untagged (or inaccurately tagged) music.

Sampling is a musical practice used extensively in hip-hop and other contemporary popular music, in which sections of one or more original tracks are combined with new audio. Sample identification, then, is the process of identifying which tracks have been sampled to create the new track. Although it would be very useful to identify samples automatically, either for copyright protection or for musicological research, this task has not been as extensively studied as genre classification and many other tasks in MIR. The approach to sample identification in \cite{van_balen_sample_2012} uses the form of audio fingerprinting developed in \cite{wang_industrial_2003}: namely, the use of "landmarks" in the audio signal to generate a hash that can be compared with the hash of an audio query in order to identify a track. Whereas most audio fingerprinting approaches assume an exact copy of a track is being played (perhaps with background noise), samples in hip-hop and other genres may be looped, pitch-shifted, and/or time-stretched, making retrieval more difficult.

These two tasks represent relatively different areas of MIR. A system that performs well on both might prove useful for an even wider variety of tasks. Thus we wish to explore whether topic models can serve as a valuable piece of such a system, given their broad utility in modeling text \cite{blei_latent_2003,hall_studying_2008,gerrish_language-based_2010}, images \cite{blei_modeling_2003,sudderth_describing_2008}, and audio \cite{hoffman_content-based_2008,hoffman_finding_2009,kim_acoustic_2009}.

In this paper, we will investigate the use of probabilistic topic models in genre classification and sample identification. In section 2, we briefly describe the formulation of these models with regard to real-valued data. In section 3, we describe the data and methods of our study. Section 4 reports our results for each combination of task, feature, and model; Section 5 contains discussion and future directions.

\section{Topic Models with a Gaussian Observation Model}

Topic models, such as latent Dirichlet allocation \cite{blei_latent_2003}, are typically unsupervised models that assume documents are made up of a mixture of "topics," with each topic being a distribution over observed features. Intuitively, the per-document topic distributions reflect the idea that each document in a corpus tends to emphasize a small handful of themes, and that not every document will contain these themes in the same proportions.

LDA is perhaps the simplest model of this type; the number of topics to learn must be set in advance. The hierarchical Dirichlet process \cite{teh_hierarchical_2006}, a non-parametric extension of LDA, has the advantage that the number of topics is not fixed and will be adjusted according to the complexity of the data.

LDA has a Dirichlet prior and a multinomial posterior, allowing only word counts as input. Gaussian-LDA, described in \cite{hu_latent_2012}, uses instead a Gaussian-Wishart prior and Gaussian posterior, permitting real-valued features as input. Gaussian-LDA operates similarly to a Gaussian mixture model, but unlike the GMM which presumes the same proportions for all documents, it draws the observations from a document-specific distribution of the available Gaussians.

The Python package \textsc{bnpy} \cite{hughes_bnpy:_2014} provides scalable inference for a variety of models, including both parametric and non-parametric topic models. \textsc{bnpy} conceptually divides each model into an allocation model and an observation model. This allows users to switch easily between finite models like LDA and infinite models like HDP, as well as discrete observations such as word counts (via the multinomial distribution) and continuous ones like most image and audio features (through a multivariate Gaussian). We make use of this flexibility in our experiments.

\section[Data and Method]{Data and Method\footnotemark}

\footnotetext{Code to reproduce these experiments can be found at \url{https://bitbucket.org/corajr/masters/src}.}

\subsection{Data}

Genre classification experiments were run on the Homburg dataset \cite{homburg_benchmark_2005}, which contains 1886 samples of 10 s each, comprising 9 different genres. We used stratified 10-fold cross validation, preserving the class proportions in training and test sets.

Sample identification used the Van Balen dataset \cite{van_balen_sample_2012}, consisting of 143 tracks of varying lengths: 68 original, or "candidate" tracks, and 75 tracks that sampled them, or "queries." Once again, stratified 10-fold cross validation is used to form training and test sets (considering original tracks as one class and sampling tracks as another).

\subsection{Features}

We extracted audio features from the data using the Python packages \textsc{Essentia} \cite{bogdanov_essentia:_2013} and \textsc{librosa} \cite{mcfee_librosa:_2015}. All audio data was converted to mono with a 22050 Hz sample rate before further processing.

The same set of features are used in both genre classification and sample identification tasks, to determine which features are most helpful for each task. In the following experiments, we chose two relatively low-level features dealing with timbre and rhythm, the combination thereof (formed by concatenating the feature vectors), and a feature drawn from the pre-trained convolutional neural network described in \cite{choi_transfer_2017}.

\subsubsection{Timbre}

Audio has several distinct perceptual dimensions that can be usefully quantified. Timbre, for example, is the "character" of a sound, as opposed to its pitch (fundamental frequencies) or rhythm (temporal organization). Timbre is largely a function of the instruments used in a piece, which provides an important signal for genre recognition.

Most MIR work uses mel-frequency cepstral coefficients (MFCCs) as a measure of timbre. These features were originally used in speech-related tasks, but have proven broadly useful as a succinct general-purpose audio descriptor in both speech and music. In this paper, however, we use gammatone-frequency cepstral coefficients (GFCCs) \cite{shao_auditory-based_2009} in lieu of MFCCs. GFCCs have been shown to be more robust to noise than MFCCs in speech applications, and have also begun to be used in MIR \cite{cai_automatic_2011,tian_music_2016}.

GFCCs are calculated similarly to MFCCs, but use a gammatone filterbank instead of a mel filterbank. The spectrum of each frame (1024 samples, or 46 ms) is passed through the filterbank; the log-power spectrum of the filter bands is fed into the discrete cosine transform (DCT) to decorrelate output. We use only dimensions 1 through 13 of this vector, discarding the DC and high-frequency components.

Many audio features, like MFCCs and GFCCs, are frame-based (calculated e.g. every 512 or 1024 samples), raising the question of how to extract higher-level features that can summarize an entire track. One common approach is to perform vector quantization on MFCCs, typically by K-means \cite{weston_latent_2012}, counting the resulting sonic "words" to produce a bag-of-frames representation for each track. Such an approach is simple and quick to implement, but has the disadvantage that document membership information is discarded when learning features. We include this K-means approach as another feature, with $K=2000$, to compare our results with this common method.

\subsubsection{Rhythm}

Rhythm is quantified using the scale transform, using a method developed in \cite{holzapfel_scale_2009}. The onset strength signal (OSS) is computed in 8 second rectangular windows over the audio signal, with a hop size of 4 s. The autocorrelation of the OSS is then passed to the scale transform with 512 bins (computed using the fast Mellin transform (FMT)), creating a 257-dimensional complex vector. Following \cite{prockup_modeling_2015}, we take the absolute value and compute the DCT to decorrelate the output, taking coefficients 1 through 60. This process creates a real-valued descriptor of rhythm which is invariant to time-stretching or changes in tempo; the hop size helps to mitigate the shift-dependence of the scale transform.

Because of the comparatively lengthy windows and long hop size, there are far fewer observations of the rhythmic feature on each track; for training purposes, the scale transform outputs are repeated until reaching the same number of observations as the GFCCs (roughly 86 copies of each observation).

\subsubsection{Convnet Feature}

Finally, along with these lower-level features we include two variants of a higher-level feature, computed by the pre-trained convolutional neural network of \cite{choi_transfer_2017}. This network comprises 5 convolutional layers with 32 feature maps each; it takes a mel-spectrogram as input, then average pools the activations from each layer to produce successively higher-level summaries of the audio. The convnet was originally trained for genre classification on a different dataset, but has demonstrated effectiveness in transfer learning across a range of datasets and tasks.

The first variant of the feature, just as with the original work in \cite{choi_transfer_2017}, is produced by concatenating the feature map activations of each layer; this yields a single 160-D vector that summarizes an entire track at multiple levels of detail. However, because there is just one "word" per document in this case, this feature is unsuitable to use as input to a topic model.

The variant we used for the topic model was created by extracting hypercolumns \cite{hariharan_hypercolumns_2015} over each "pixel" of the mel-spectrogram, then taking the hypercolumns' maximum across all feature maps. The resulting 96-D feature vectors (1 per frame) are essentially a selective amplification of the input, where parts of the spectrum that activated the neural network are given higher values while the rest tend toward zero. (Using the full hypercolumn ($160*96=15360$D) was impractical for our case; other variations, such as taking the mean of the hypercolumns across all frames or using only the feature maps from the highest-level layer, produced substantially worse results in preliminary experiments and are omitted here.)

\subsection{Models}

All topic models were trained using \textsc{bnpy}. For each real-valued feature, LDA with a full-covariance Gaussian observation model was trained at $K=10$ and $K=50$, and HDP with full-covariance Gaussian was trained with $K_{init}=100$.\footnote{With the HDP model, the hypercolumn CNN feature took 8 hours to train for a single cross-validation fold (1 out of 10) on available hardware; it was hence omitted as impractical.} We also trained standard (count-based) LDA with 50 topics on the K-means bag-of-frames vector, to compare with a more typical feature in this setting.

To translate the topic model output into genre predictions, a linear SVM classifier was trained on the topic proportions for each document in the training set, and accuracy was then computed for the test set. The classifier was also trained on the document-level mean of each feature (or on the single document-level feature, in the CNN's case), to assess how much the underlying feature was responsible for performance.

Sample identification was undertaken by comparing the topic proportions of each document via cosine similarity to find the closest candidate tracks, after which the mean average precision (MAP) was computed. Similarly, the cosine similarity was also calculated for the document-level mean of each feature, the CNN output, and the GFCC bag-of-frames vector.

\section{Results}

Top performance on each task is given in \textbf{bold}; reported values are the mean percentages over the cross-validation splits (standard deviations are listed in parentheses). The following abbreviations are used in the table:

\begin{description}
    \item[Random] Baseline (100 repetitions)
    \item[Mean] Track-level mean of the feature described
    \item[GaussLDA-K] LDA of $K$ components with Gaussian observation model
    \item[GaussHDP-K] HDP of $K$ initial components with Gaussian observation model
    \item[LDA-K] LDA of $K$ components with multinomial observation model
    \item[CNN] Convolutional neural net
    \item[Cos] Cosine similarity-based retrieval
    \item[SVM] Linear SVM
\end{description}

\begin{table}[h]
\input{../out/genre_table}
\end{table}

\subsection{Genre Classification}

The original paper \cite{homburg_benchmark_2005} used 49 features, achieving a top accuracy of 53.23\% using a $k$-nearest neighbor method with an adaptive distance metric. State of the art performance is around 63.46\% \cite{panagakis_music_2014}.

Table~\ref{genre-table} shows our results for genre classification. The top result was 62.04\%, from the document-level CNN feature fed directly into the linear SVM (with no topic model).
\begin{table}[h]
\input{../out/sample_table}
\end{table}

\subsection{Sample Identification}

In the original paper \cite{van_balen_sample_2012}, an audio fingerprinting system was used to extract a hash from each track and then cosine similarity was used to retrieve candidates. In addition, "noise" tracks were added to challenge the system in the original work, but these were not available at the time of this writing. The original authors' system achieved a mean average precision (MAP) of 22.8\% for the basic system, and 39.0\% for a modified form including pitch-shifted tracks as candidates.

Table~\ref{sample-table} shows our results for sample identification. The top result was 12.48\%, achieved by the GFCC bag-of-frames feature and a linear SVM (with no topic model).

\section{Discussion and Conclusion}

In the genre task, topic models managed to effectively capture latent factors in the dataset and achieve moderate performance, but were ultimately outperformed by the pre-trained convnet with a linear classifier. The GFCC (timbral) feature was the most effective input to the topic models at $K=50$, producing a result of 51.00\% accuracy with the CNN hypercolumn feature close behind. (This is somewhat lower than the 53\% baseline of the original paper that used 49 features, but is fairly good performance for using a single feature.) Comparing results from feature means, the GFCC and concatenated GFCC/scale transform were comparable to each other, hovering around 46\%. Using the CNN's document-level feature on its own produced the top result of 62.04\%, which is close to state of the art for this dataset.

Sample identification proved substantially more difficult for all the models used. This may be due to the underlying assumption that only one topic is present at a given time point (violated when the samples are mixed together), or because of the trivial method of cosine similarity used to retrieve candidate tracks. Nevertheless, this is a difficult task and there have not yet been any particularly impressive results on this dataset. Comparing the sample results using the raw feature means, the scale transform (rhythmic feature) gave the best results. In our experiments, the top performance was given by running a linear SVM on the K-means bag-of-frames representation, although results were very close to the CNN feature (both the hypercolumn feature with 50-topic LDA and the document-level version).

On the sample task, the concatenated timbral and rhythmic feature showed improvement over timbre alone, while on the genre task, the combined feature generally did worse. This likely reflects a difference in the underlying datasets as well as the tasks; the sample ID dataset is mostly comprised of jazz and hip-hop, both of which tend to emphasize rhythmic organization, while the genre dataset spans many genres that are most readily differentiated not by rhythm but by the different musical instruments used (and hence, the different timbres present).

In both tasks, the use of HDP to fit the number of topics to the data did not show substantial improvement over using a fixed numbers of topics: the number of topics was reduced only by 10 or so from the initial 100, and performance was often worse than the LDA trained at $K=50$. More tuning of hyperparameters might have helped to improve this result.

It is clear that the performance of the topic model is heavily dependent on the underlying feature(s). This is evident from the fact that the performance of simply taking the mean of the feature across the entire track was in each case similar to the result of first training the topic model on the hundreds or thousands of observations from each track. Further, timbral features appear to be more important for the success of genre classification, and rhythmic features for sample identification, perhaps stemming from the difference in datasets noted above.

Predictive performance of the convnet feature when fed directly to the SVM exceeded the CNN + topic model + SVM combination in nearly every case. This is unsurprising, as the convnet is optimized specifically for good discriminative performance in genre classification, while the topic model is optimizing an unrelated objective (the posterior probability of the documents). In general, using unsupervised topic modeling (or any technique) for dimensionality reduction often loses information that would aid discriminative tasks, as illustrated by the original LDA paper's experiments with text classification \cite{blei_latent_2003}. This loss could be mitigated by using a supervised topic model, such as sLDA \cite{blei_supervised_2008}, which models a response variable jointly with document content. Such a supervised model would learn a representation that has better predictive power for the task at hand.

This paper demonstrates that unsupervised topic models can provide a viable low-dimensional representation of audio without labeled data, although their ultimate performance depends strongly on input features. The convnet feature in particular is versatile, obtaining good performance on both tasks, and at present often works better on its own than as an input to the topic model. Future work could explore the use of a supervised topic model (ideally one that can accommodate real-valued input data), or a fully hybrid neural network/topic model with end-to-end discriminative training, as in \cite{wan_hybrid_2012}. Such a model would presumably improve performance by richly capturing the details of the underlying data via the neural net, while also leveraging the topic model's structure to capture a higher-level representation of features as they are differently distributed across documents.

\bibliographystyle{unsrt}
\bibliography{johnsonroberson}

\end{document}
