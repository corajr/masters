
# Paths to these two dependencies must be set correctly!
BNPY_DIR := ~/Documents/bnpy-dev/
KMEANS_REX_DIR := ~/Documents/KMeansRex/python

export PYTHONPATH := ${PYTHONPATH}:$(BNPY_DIR):$(KMEANS_REX_DIR)
export BNPYOUTDIR := $(abspath out/models)
export BNPYDATADIR := $(abspath out/loaders)

PYTHON := python

HOMBURG := $(wildcard data/homburg/*/*.mp3)
VANBALEN := $(wildcard data/vanbalen/*.wav)

HOMBURG_DATA := out/homburg.h5
VANBALEN_DATA := out/vanbalen.h5

HOMBURG_METADATA := out/homburg_genres.json
VANBALEN_METADATA := out/vanbalen_samples.json
VANBALEN_SAMPLE_INTERVALS := out/vanbalen_sample_intervals.pkl

VANBALEN_METADATA_PAT := out/vanbalen_samples%json
VANBALEN_SAMPLE_INTERVALS_PAT := out/vanbalen_sample_intervals%pkl

HOMBURG_TABLE := out/genre_table.tex
VANBALEN_TABLE := out/sample_table.tex
TABLES_PAT := out/genre_table%tex out/sample_table%tex

PAPER := paper/johnsonroberson.pdf
PAPER_TEX := paper/johnsonroberson.tex

GFCCS := GFCCs
SCALE := Scale
GFCCS_SCALE := GFCCsScale
GFCCS_KMEANS := GFCCsKmeans
CNN := CNN

FEATURE_TYPES := $(GFCCS) $(SCALE) $(GFCCS_SCALE) $(CNN)
MODEL_TYPES := LDA_10_GAUSS LDA_50_GAUSS HDP_100_GAUSS

OUT_DIR := out
MODEL_DIR := out/models
LOADERS_DIR := out/loaders

define OUT_FOR_MODEL_template =
$(1)_$(3)_OUT := $$(addprefix out/$(2)_$(3)_, $$(addsuffix .json, $$(FEATURE_TYPES)))

endef

define OUT_template =
$(foreach model_type,$(MODEL_TYPES),$(call OUT_FOR_MODEL_template,$(1),$(2),$(model_type)))
$(1)_OUT := $(foreach model_type,$(MODEL_TYPES),$$($(1)_$(model_type)_OUT)) out/$(2)_kmeans_GFCCsKmeans.json
endef

$(eval $(call OUT_template,HOMBURG,homburg))
$(eval $(call OUT_template,VANBALEN,vanbalen))

SPLITS_N := 0 1 2 3 4 5 6 7 8 9

define SPLITS_DTYPE_template =
$(1)_SPLITS_PY_$(3) := $$(addprefix out/loaders/$(2)CorpusSplit$(3),$$(addsuffix .py, $$(SPLITS_N)))
$(1)_SPLITS_PY_$(3)_PAT := $$(addprefix out/loaders/$(2)CorpusSplit$(3),$$(addsuffix %py, $$(SPLITS_N)))

endef

define MODEL_template =
$(1)_SPLITS_$(3)_DIRS = $$(addprefix out/models/$(2)CorpusSplit$$*,$$(addsuffix /$(4)/1/,$$(SPLITS_N)))
$(1)_SPLITS_$(3)_OUT = $$(addsuffix BestAllocModel.mat, $$($(1)_SPLITS_$(3)_DIRS))
$(1)_SPLITS_$(3)_DIRS_PAT = $$(addprefix out/models/$(2)CorpusSplit%,$$(addsuffix /$(4)/1/,$$(SPLITS_N)))
$(1)_SPLITS_$(3)_OUT_PAT = $$(addsuffix BestAllocModel.mat, $$($(1)_SPLITS_$(3)_DIRS_PAT))
endef


define SPLITS_template =
$(1)_SPLITS := $$(addprefix out/$(3)_splits/$(3),$$(addsuffix .h5, $$(SPLITS_N)))
$(1)_SPLITS_PAT := $$(addprefix out/$(3)_splits/$(3),$$(addsuffix %h5, $$(SPLITS_N)))
$(1)_SPLITS_GFCCsKmeans := $$(addprefix out/$(3)_splits/$(3),$$(addsuffix _GFCCs_kmeans.pkl, $$(SPLITS_N)))
$(1)_SPLITS_GFCCsKmeans_PAT := $$(addprefix out/$(3)_splits/$(3),$$(addsuffix _GFCCs_kmeans%pkl, $$(SPLITS_N)))
$(foreach feat,$(FEATURE_TYPES) $(GFCCS_KMEANS),$(call SPLITS_DTYPE_template,$(1),$(2),$(feat)))
$(call MODEL_template,$(1),$(2),HDP_100_GAUSS,hdp100-gauss-laps100)
$(call MODEL_template,$(1),$(2),LDA_50_GAUSS,lda50-gauss-laps100)
$(call MODEL_template,$(1),$(2),LDA_50_MULT,lda50-mult-laps100)
$(call MODEL_template,$(1),$(2),LDA_10_GAUSS,lda10-gauss-laps100)
endef

$(eval $(call SPLITS_template,HOMBURG,Homburg,homburg))
$(eval $(call SPLITS_template,VANBALEN,Vanbalen,vanbalen))

HOMBURG_CNN_TOPICS := $(foreach model,$(MODEL_TYPES),out/homburg_$(model)_CNN.json)
VANBALEN_CNN_TOPICS := $(foreach model,$(MODEL_TYPES),out/vanbalen_$(model)_CNN.json)

.PHONY: all extract splits genre paper sample_id homburg_splits vanbalen_splits cnn cnn_topics clean_cnn backup_json

.SECONDARY:

.PRECIOUS: $(HOMBURG_DATA) $(VANBALEN_DATA)

all: genre sample_id paper

genre: $(HOMBURG_OUT)

sample_id: $(VANBALEN_OUT)

paper: $(PAPER)

extract: $(HOMBURG_DATA) $(VANBALEN_DATA)

timestamp := $(shell /bin/date "+%Y-%m-%d--%H-%M-%S")

backup_json:
	tar -czf ../masters_results/out-$(timestamp).tar.gz out/*.json

clean_cnn:
	rm -rf out/*_splits/*CNN*
	rm -rf out/models/*CNN*

cnn_topics: $(HOMBURG_CNN_TOPICS) $(VANBALEN_CNN_TOPICS)

cnn_direct: out/homburg_cnn_CNN.json out/vanbalen_cnn_CNN.json

cnn: cnn_topics cnn_direct

$(OUT_DIR): | $(BNPY_DIR) $(KMEANS_REX_DIR)
	mkdir -p out

$(MODEL_DIR): | $(OUT_DIR)
	mkdir -p out/models

$(LOADERS_DIR): | $(OUT_DIR)
	mkdir -p out/loaders

$(PAPER): $(PAPER_TEX) $(HOMBURG_TABLE) $(VANBALEN_TABLE)
	cd paper && pdflatex johnsonroberson.tex && \
		bibtex johnsonroberson && pdflatex johnsonroberson.tex

OUT_FILES := $(HOMBURG_OUT) $(VANBALEN_OUT)
OUT_FILES_READY := $(wildcard out/*.json)

$(TABLES_PAT): $(filter $(OUT_FILES), $(OUT_FILES_READY))
	$(PYTHON) src/make_tables.py $(HOMBURG_TABLE) $(VANBALEN_TABLE)

# generating results

define OUT_FILES_template =
out/$(2)_$(3)_%.json: $$($(1)_SPLITS_$(3)_OUT_PAT)
	$(PYTHON) src/$(4).py $$@ $$($(1)_SPLITS_$(3)_DIRS) $$($(1)_SPLITS)
endef

$(foreach model_type,$(MODEL_TYPES),$(eval $(call OUT_FILES_template,VANBALEN,vanbalen,$(model_type),identify_samples)))
$(foreach model_type,$(MODEL_TYPES),$(eval $(call OUT_FILES_template,HOMBURG,homburg,$(model_type),classify)))

out/vanbalen_kmeans_%.json: $(VANBALEN_SPLITS_LDA_50_MULT_OUT_PAT) $(VANBALEN_SPLITS_GFCCS_KMEANS) $(VANBALEN_SPLITS_GFCCsKmeans_PAT)
	$(PYTHON) src/identify_samples.py $@ $(VANBALEN_SPLITS_LDA_50_MULT_DIRS) $(VANBALEN_SPLITS)

out/homburg_kmeans_%.json: $(HOMBURG_SPLITS_LDA_50_MULT_OUT_PAT) $(HOMBURG_SPLITS_GFCCS_KMEANS) $(HOMBURG_SPLITS_GFCCsKmeans_PAT)
	$(PYTHON) src/classify.py $@ $(HOMBURG_SPLITS_LDA_50_MULT_DIRS) $(HOMBURG_SPLITS)

out/homburg_cnn_CNN.json: $(HOMBURG_SPLITS) src/nn.py
	$(PYTHON) src/nn.py $@ $(HOMBURG_SPLITS)

out/vanbalen_cnn_CNN.json: $(VANBALEN_SPLITS) src/nn.py
	$(PYTHON) src/nn.py $@ $(VANBALEN_SPLITS)

# reading metadata

$(VANBALEN_METADATA_PAT) $(VANBALEN_SAMPLE_INTERVALS_PAT): data/vanbalen/samples.csv $(VANBALEN) src/vanbalen_samples.py | $(OUT_DIR)
	$(PYTHON) src/vanbalen_samples.py $@ $<

$(HOMBURG_METADATA): $(HOMBURG) src/group_classes.py | $(OUT_DIR)
	$(PYTHON) src/group_classes.py $@

# training topic models

out/models/%/lda10-gauss-laps100/1/BestAllocModel.mat: out/loaders/%.py | $(MODEL_DIR)
	$(PYTHON) -m bnpy.Run $* FiniteTopicModel Gauss moVB --K 10 --initname kmeans --nLap 100 --nWorkers 4 --printEvery 5 --nTask 1 --jobname lda10-gauss-laps100

out/models/%/lda50-gauss-laps100/1/BestAllocModel.mat: out/loaders/%.py | $(MODEL_DIR)
	$(PYTHON) -m bnpy.Run $* FiniteTopicModel Gauss moVB --K 50 --initname kmeans --nLap 100 --nWorkers 4 --printEvery 5 --nTask 1 --jobname lda50-gauss-laps100

out/models/%/lda50-mult-laps100/1/BestAllocModel.mat: out/loaders/%.py | $(MODEL_DIR)
	$(PYTHON) -m bnpy.Run $* FiniteTopicModel Mult moVB --K 50 --initname kmeansplusplus --nLap 100 --nWorkers 4 --printEvery 5 --nTask 1 --jobname lda50-mult-laps100

# dset is small ~128: d[elete]targetMaxSize should be small
out/models/Vanbalen%/hdp100-gauss-laps100/1/BestAllocModel.mat: out/loaders/Vanbalen%.py | $(MODEL_DIR)
	$(PYTHON) -m bnpy.Run Vanbalen$* HDPTopicModel Gauss moVB --K 100 --initname kmeans --moves merge,delete --mergePerLap 25 --mergePairSelection corr --dtargetMaxSize 10 --nLap 100 --nWorkers 4 --printEvery 5 --nTask 1 --jobname hdp100-gauss-laps100

# dset is ~1000 docs
out/models/Homburg%/hdp100-gauss-laps100/1/BestAllocModel.mat: out/loaders/Homburg%.py | $(MODEL_DIR)
	$(PYTHON) -m bnpy.Run Homburg$* HDPTopicModel Gauss moVB --K 100 --initname kmeans --moves merge,delete --mergePerLap 25 --mergePairSelection corr --dtargetMaxSize 100 --nLap 100 --nWorkers 4 --printEvery 5 --nTask 1 --jobname hdp100-gauss-laps100

# extracting basic features from audio

$(HOMBURG_DATA): src/proc_all_tracks.py src/scale_transform.py src/add_nn_features.py src/nn.py $(HOMBURG) | $(OUT_DIR)
	$(PYTHON) src/proc_all_tracks.py $@ $(HOMBURG)
	$(PYTHON) src/add_nn_features.py $@ $(HOMBURG)

$(VANBALEN_DATA): src/proc_all_tracks.py src/scale_transform.py src/add_nn_features.py src/nn.py $(VANBALEN) | $(OUT_DIR)
	$(PYTHON) src/proc_all_tracks.py $@ $(VANBALEN)
	$(PYTHON) src/add_nn_features.py $@ $(VANBALEN)

# cross-validation splits

splits: homburg_splits vanbalen_splits

homburg_splits: $(HOMBURG_SPLITS_PY_GFCCS) $(HOMBURG_SPLITS_PY_GFCCS_SCALE) $(HOMBURG_SPLITS_PY_GFCCS_KMEANS)

vanbalen_splits: $(VANBALEN_SPLITS_PY_GFCCS) $(VANBALEN_SPLITS_PY_GFCCS_SCALE) $(HOMBURG_SPLITS_PY_GFCCS_KMEANS)

define SPLIT_LOADER_template =
$$($(1)_SPLITS_PY_$(3)_PAT): src/CorpusSplit.py src/bnpy_util.py $$($(1)_SPLITS) | $$(LOADERS_DIR)
	$$(foreach var,$$($(1)_SPLITS_PY_$(3)),cp -f src/CorpusSplit.py $$(var);)
endef

define GFCCsKmeans_SPLIT_LOADER_template =
$$($(1)_SPLITS_PY_$(3)_PAT): src/CorpusSplit.py src/bnpy_util.py $$($(1)_SPLITS) $$($(1)_SPLITS_GFCCsKmeans) | $$(LOADERS_DIR)
	$$(foreach var,$$($(1)_SPLITS_PY_$(3)),cp -f src/CorpusSplit.py $$(var);)
endef

$(foreach feat,$(FEATURE_TYPES),$(eval $(call SPLIT_LOADER_template,HOMBURG,Homburg,$(feat))))
$(foreach feat,$(FEATURE_TYPES),$(eval $(call SPLIT_LOADER_template,VANBALEN,Vanbalen,$(feat))))

$(eval $(call GFCCsKmeans_SPLIT_LOADER_template,VANBALEN,Vanbalen,GFCCsKmeans))
$(eval $(call GFCCsKmeans_SPLIT_LOADER_template,HOMBURG,homburg,GFCCsKmeans))


define SPLIT_MAKE_template =
$(1)_SPLITS_DIR := out/$(2)_splits
$$($(1)_SPLITS_DIR): | $$(OUT_DIR)
	mkdir -p out/$(2)_splits

$$($(1)_SPLITS_GFCCsKmeans_PAT): $$($(1)_SPLITS) src/kmeans.py
	$$(PYTHON) src/kmeans.py $$($(1)_SPLITS)

$$($(1)_SPLITS_PAT): $$($(1)_DATA) src/split.py $$($(1)_METADATA) | $$($(1)_SPLITS_DIR)
	$$(PYTHON) src/split.py $$($(1)_DATA) $$($(1)_SPLITS)
endef

$(eval $(call SPLIT_MAKE_template,HOMBURG,homburg))
$(eval $(call SPLIT_MAKE_template,VANBALEN,vanbalen))
