#!/usr/bin/env bash

ESSENTIA_VERSION="v2.1_beta2"
BASE="$HOME"

get_or_clone() {
    cd $BASE
    if [ ! -d "$1" ]; then
        git clone "$2"
    fi
}

install_essentia() {
    get_or_clone essentia https://github.com/MTG/essentia.git
    cd $BASE/essentia
    git branch --list | grep "* $ESSENTIA_VERSION" || git checkout "$ESSENTIA_VERSION"
    ./waf configure --mode=release --with-python --prefix=$HOME/.local 2>&1
    ./waf 2>&1
    ./waf install 2>&1
}

install_kmeans_rex() {
    get_or_clone KMeansRex https://github.com/michaelchughes/KMeansRex
    cd $BASE/KMeansRex
    make python
}

install_kapre() {
    get_or_clone kapre https://github.com/keunwoochoi/kapre.git
    cd $BASE/kapre
    git checkout a3bde3e
    python setup.py install --user
}


install_compact_cnn() {
    get_or_clone music-auto_tagging-keras https://github.com/corajr/music-auto_tagging-keras.git
    cd $BASE/music-auto_tagging-keras
    git checkout 5fe55d0
    touch compact_cnn/__init__.py
}

install_libsamplerate() {
    cd $BASE
    wget http://www.mega-nerd.com/SRC/libsamplerate-0.1.9.tar.gz
    tar -xzf libsamplerate-0.1.9.tar.gz
    cd libsamplerate-0.1.9
    ./configure --disable-sndfile --prefix=$HOME/.local
    make
    make install
    cd ..
    rm libsamplerate-0.1.9.tar.gz
}

install_yaml() {
    cd $BASE
    wget http://pyyaml.org/download/libyaml/yaml-0.1.7.tar.gz
    tar -xzf yaml-0.1.7.tar.gz
    cd yaml-0.1.7
    ./configure --prefix=$HOME/.local
    make
    make install
    cd ..
    rm yaml-0.1.7.tar.gz
}

check_or_add_env_vars() {
    grep 'PYTHONPATH' ~/.bash_profile || echo 'export PYTHONPATH=${PYTHONPATH}:$HOME/bnpy-dev/:$HOME/KMeansRex/python:$HOME/music-auto_tagging-keras' >> ~/.bash_profile
    grep 'LD_LIBRARY_PATH' ~/.bash_profile || echo 'export LD_LIBRARY_PATH=$HOME/.local/lib:$LD_LIBRARY_PATH' >> ~/.bash_profile
    grep 'PKG_CONFIG_PATH' ~/.bash_profile || echo 'export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/gpfs/runtime/opt/eigen/3.2.1/share/pkgconfig:$HOME/.local/lib/pkgconfig' >> ~/.bash_profile
    . ~/.bash_profile
}

check_or_add_env_vars

pip install --user -r requirements.txt
python -c 'import essentia' || install_essentia
python -c 'import KMeansRex' || install_kmeans_rex
python -c 'import kapre' || install_kapre
python -c 'import compact_cnn.main' || install_compact_cnn
pkg-config samplerate || install_libsamplerate
pkg-config yaml-0.1 || install_yaml
get_or_clone bnpy-dev https://michaelchughes@bitbucket.org/michaelchughes/bnpy-dev/
