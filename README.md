# Cora Johnson-Roberson Masters Work

[Read the paper](https://bitbucket.org/corajr/masters/downloads/johnsonroberson.pdf)

To run the experiments:

```
make
```

## Requirements

This is intended to be run using the Canopy Enthought Python Distribution.

The following Python packages should be added to the default distribution:
* progressbar
* h5py
* scikit_learn
* librosa (use pip)
* keras (pip)
* intervaltree (pip)
* [KMeansRex](https://github.com/michaelchughes/KMeansRex)
* Essentia ([install instructions](http://essentia.upf.edu/documentation/installing.html))

Essentia's install prefix must be set to Enthought's distribution dir, e.g.
  `--prefix=$HOME/Enthought/Canopy_64bit/User` should be added to `waf configure`.

[bnpy](https://bitbucket.org/michaelchughes/bnpy-dev/wiki/Installation.md) and KMeansRex must be placed on the global PYTHONPATH like so, in your `.bashrc`:

```
export PYTHONPATH=${PYTHONPATH}:$HOME/Documents/bnpy-dev/:$HOME/Documents/KMeansRex/python
```

Finally, TeXLive must be installed to generate the paper:

```
sudo apt install texlive
sudo apt install texlive-latex-extra
```


-----

## Usage on CCV

To install on CCV, include the following in your `.modules` file:

```
module load git/2.10.0
module load eigen
module load fftw
module load hdf5
module load tensorflow
module load ffmpeg/1.2
```

Then:

```sh
source ~/.modules
git clone https://bitbucket.org/corajr/masters.git
cd masters
bash ./local-provision.sh
```
